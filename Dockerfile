FROM node:15

RUN apt-get update \
  && apt-get install -y postgresql postgresql-contrib \
  && apt-get install sudo \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir /app
WORKDIR /app
COPY package.json pnpm-lock.yaml /app/

RUN npm i -g @adonisjs/cli
RUN npm install

COPY .env.example .env
COPY . /app

EXPOSE 3333
EXPOSE 5432

CMD [ "npm", "start" ]